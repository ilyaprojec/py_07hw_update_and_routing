from django import forms
from core.models import Group, Student


class GroupForm(forms.ModelForm):

    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'students': forms.widgets.CheckboxSelectMultiple()
        }


class StudentForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = '__all__'
