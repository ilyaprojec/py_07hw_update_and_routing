from django.views.generic import CreateView, UpdateView
from django.views.generic.base import TemplateView
from django.views.generic import FormView
from django.shortcuts import redirect
from django.db.models import Avg, Max, Min, Count
from core.forms import GroupForm, StudentForm
from django.urls.base import reverse_lazy
from core.models import Student, Group


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        groups = Group.objects.all().values('id', 'name', 'teacher_name').annotate(
            student_count=Count('students'),
            student_avg=Avg('students__age'),
            student_max=Max('students__age'),
            student_min=Min('students__age'),
        )

        return {
                'groups': groups,
            }


class TeachersView(TemplateView):
    template_name = "teachers.html"

    def get_context_data(self, **kwargs):
        groups = Group.objects.all().values('id', 'name', 'teacher_name')

        return {
            'groups': groups
        }


class StudentsView(TemplateView):
    template_name = "students.html"

    def get_context_data(self, **kwargs):
        students = Student.objects.all()
        print(students)
        return {
            'students': students
        }


class StudentCreateView(CreateView):
    template_name = 'create.html'
    success_url = reverse_lazy('main:teachers')
    model = Student
    fields = '__all__'


class GroupCreateView(FormView):
    template_name = 'create.html'
    form_class = GroupForm
    success_url = reverse_lazy('main:teachers')

    def form_valid(self, form):
        form.save()
        return super(GroupCreateView, self).form_valid(form)


class StudentUpdateView(UpdateView):
    template_name = 'update_student.html'

    success_url = reverse_lazy('main:students')
    model = Student
    form_class = StudentForm
    pk_url_kwarg = 'student_id'


class GroupUpdateView(UpdateView):
    template_name = 'update_group.html'

    success_url = reverse_lazy('main:teachers')
    model = Group
    form_class = GroupForm
    pk_url_kwarg = 'group_id'


def delete_student(request, student_id=None):
    post_to_delete = Student.objects.get(id=student_id)
    post_to_delete.delete()

    return redirect('main:home')


def delete_group(request, group_id=None):
    post_to_delete = Group.objects.get(id=group_id)
    post_to_delete.delete()

    return redirect('main:teachers')
