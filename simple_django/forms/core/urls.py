# from core.views import IndexView, CreateStudentView, CreateGroupView,
from django.urls import path
from core.views import IndexView, StudentCreateView, GroupCreateView, GroupUpdateView, StudentUpdateView, TeachersView, StudentsView, delete_student, delete_group

urlpatterns = [
    path('', IndexView.as_view(), name='home'),
    path('teachers/', TeachersView.as_view(), name='teachers'),
    path('students/', StudentsView.as_view(), name='students'),
    path('create-student/', StudentCreateView.as_view(), name='create-student'),
    path('create-group/', GroupCreateView.as_view(), name='create-group'),
    path('update-group/<int:group_id>/', GroupUpdateView.as_view(), name='update-group'),
    path('update-student/<int:student_id>/', StudentUpdateView.as_view(), name='update-student'),
    path('delete-student/<int:student_id>/', delete_student, name='delete-student'),
    path('delete-group/<int:group_id>/', delete_group, name='delete-group'),
]
